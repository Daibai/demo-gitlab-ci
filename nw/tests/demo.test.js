module.exports = {
 "Test démo" : function (browser) {
   browser
   .url("http://www.babonaux.com/enzymedemo")
   .waitForElementVisible('body', 2000)
   .expect.element('#root').to.be.present;
   browser.end();
  }
};